import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class BFSExample {

    private Queue<Node> queue;
    static ArrayList<Node> nodes = new ArrayList<Node>();

    static class Node{
        int data;
        boolean visited;
        List<Node> neighbours;

        Node(int data){
            this.data = data;
            this.neighbours = new ArrayList<>();
        }

        public void addNeighbours(Node neighbourNode){
            this.neighbours.add(neighbourNode);
        }

        public List<Node> getNeighbours(){
            return neighbours;
        }

        public void setNeighbours(List<Node> neighbours){
            this.neighbours = neighbours;
        }

    }

    public BFSExample(){
        queue = new LinkedList<Node>();
    }

    //Adjacenecy matrix technique
    public ArrayList<Node> findNeigbours(int adjacency_matrix[][], Node x){
        int nodeIndex =- 1;

        ArrayList<Node> neighbours = new ArrayList<Node>();
        for (int i = 0; i < nodes.size(); i++){
            if (nodes.get(i).equals(x)){
                nodeIndex = i;
                break;
            }
        }

        if (nodeIndex != -1){
            for (int j = 0; j < adjacency_matrix[nodeIndex].length; j++){
                if (adjacency_matrix[nodeIndex][j] == 1){
                    neighbours.add(nodes.get(j));
                }
            }
        }
        return neighbours;
    }

    public void bfs(int adjacency_matrix[][], Node node){

        queue.add(node);
        node.visited = true;

        while (!queue.isEmpty()){
            Node element = queue.remove();
            System.out.print(element.data + " ");
            ArrayList<Node> neighbour = findNeigbours(adjacency_matrix, element);

            for (int i = 0; i < neighbour.size(); i++){
                Node n = neighbour.get((i));

                if (n != null && !n.visited){
                    queue.add(n);
                    n.visited = true;
                }
            }
        }

    }

    //End of Adjacency Technique

    public void bfs(Node node){
        queue.add(node);
        node.visited = true;

        while (!queue.isEmpty()){
            Node element = queue.remove();
            System.out.print(element.data + " ");
            List<Node> neighbours = element.getNeighbours();

            for (int i = 0; i < neighbours.size(); i++){
                Node n = neighbours.get(i);

                if (n != null && !n.visited){
                    queue.add(n);
                    n.visited = true;
                }
            }
        }
    }

    public static void main(String arg[]){
        Node node40 = new Node(40);
        Node node10 = new Node(10);
        Node node20 = new Node(20);
        Node node30 = new Node(30);
        Node node50 = new Node(50);
        Node node60 = new Node(60);
        Node node70 = new Node(70);

        //Activate this for adjacency matrix technique

        nodes.add(node40);
        nodes.add(node10);
        nodes.add(node20);
        nodes.add(node30);
        nodes.add(node50);
        nodes.add(node60);
        nodes.add(node70);

        int adjacency_matrix[][] = {
                {0,1,1,0,0,0,0}, // Node 1: 40
                {0,0,0,1,0,0,0}, // Node 2 :10
                {0,1,0,1,1,1,0}, // Node 3: 20
                {0,0,0,0,1,0,0}, // Node 4: 30
                {0,0,0,0,0,0,1}, // Node 5: 60
                {0,0,0,0,0,0,1}, // Node 6: 50
                {0,0,0,0,0,0,0}, // Node 7: 70
        };

        //Activate this for neighbours list technique
//        node40.addNeighbours(node10);
//        node40.addNeighbours(node20);
//        node10.addNeighbours(node30);
//        node20.addNeighbours(node10);
//        node20.addNeighbours(node30);
//        node20.addNeighbours(node60);
//        node20.addNeighbours(node50);
//        node30.addNeighbours(node60);
//        node60.addNeighbours(node70);
//        node50.addNeighbours(node70);

        System.out.println("The BFS traversal of the graph is : ");
        BFSExample bfsExample = new BFSExample();
        bfsExample.bfs(adjacency_matrix, node40);
    }

}
